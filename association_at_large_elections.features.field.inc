<?php
/**
 * @file
 * association_at_large_elections.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function association_at_large_elections_field_default_fields() {
  $fields = array();

  // Exported field: 'election_candidate-candidate-field_about_me'.
  $fields['election_candidate-candidate-field_about_me'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_about_me',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'allowed_values' => '',
        'allowed_values_php' => '',
        'max_length' => '',
        'text_processing' => '1',
      ),
      'translatable' => '0',
      'type' => 'text_long',
      'type_name' => 'nomination',
    ),
    'field_instance' => array(
      'bundle' => 'candidate',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Tell us about yourself! Your background, how you got into Drupal, etc. ',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '10',
        ),
        'details' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '7',
        ),
        'full' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '4',
        ),
      ),
      'entity_type' => 'election_candidate',
      'field_name' => 'field_about_me',
      'label' => 'About Me',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '4',
        ),
        'type' => 'text_textarea',
        'weight' => '5',
      ),
    ),
  );

  // Exported field: 'election_candidate-candidate-field_candidate_comments'.
  $fields['election_candidate-candidate-field_candidate_comments'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_candidate_comments',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'reply',
      'settings' => array(
        'access' => '-1',
        'allow_reply' => '-1',
        'bundle' => 'candidate_comments',
        'display' => '-1',
        'form' => '-1',
      ),
      'translatable' => '0',
      'type' => 'reply',
    ),
    'field_instance' => array(
      'bundle' => 'candidate',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
        'details' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'full' => array(
          'label' => 'above',
          'module' => 'reply',
          'settings' => array(),
          'type' => 'reply_default',
          'weight' => '7',
        ),
      ),
      'entity_type' => 'election_candidate',
      'field_name' => 'field_candidate_comments',
      'label' => 'Comments',
      'required' => 0,
      'settings' => array(
        'access' => '-1',
        'allow_reply' => '-1',
        'display' => '-1',
        'form' => '-1',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'reply',
        'settings' => array(),
        'type' => 'reply',
        'weight' => '12',
      ),
    ),
  );

  // Exported field: 'election_candidate-candidate-field_election_picture'.
  $fields['election_candidate-candidate-field_election_picture'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_election_picture',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'candidate',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'image',
          'settings' => array(
            'image_link' => 'content',
            'image_style' => 'thumbnail',
          ),
          'type' => 'image',
          'weight' => '2',
        ),
        'details' => array(
          'label' => 'hidden',
          'module' => 'image',
          'settings' => array(
            'image_link' => '',
            'image_style' => 'medium',
          ),
          'type' => 'image',
          'weight' => '6',
        ),
        'full' => array(
          'label' => 'hidden',
          'module' => 'image',
          'settings' => array(
            'image_link' => '',
            'image_style' => 'medium',
          ),
          'type' => 'image',
          'weight' => '3',
        ),
      ),
      'entity_type' => 'election_candidate',
      'field_name' => 'field_election_picture',
      'label' => 'Picture',
      'required' => 0,
      'settings' => array(
        'alt_field' => 0,
        'default_image' => 0,
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'title_field' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => '4',
      ),
    ),
  );

  // Exported field: 'election_candidate-candidate-field_experience'.
  $fields['election_candidate-candidate-field_experience'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_experience',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'allowed_values' => '',
        'allowed_values_php' => '',
        'max_length' => '',
        'text_processing' => '1',
      ),
      'translatable' => '0',
      'type' => 'text_long',
      'type_name' => 'nomination',
    ),
    'field_instance' => array(
      'bundle' => 'candidate',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'What Drupal community contributions have you take part in (code, camps, etc.)? Do you have experience in financial oversight, developing business strategies, or organization governance? ',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '8',
        ),
        'details' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '9',
        ),
        'full' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '6',
        ),
      ),
      'entity_type' => 'election_candidate',
      'field_name' => 'field_experience',
      'label' => 'Experience',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '4',
        ),
        'type' => 'text_textarea',
        'weight' => '7',
      ),
    ),
  );

  // Exported field: 'election_candidate-candidate-field_irc_handle'.
  $fields['election_candidate-candidate-field_irc_handle'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_irc_handle',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'candidate',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '7',
        ),
        'details' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '5',
        ),
        'full' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '2',
        ),
      ),
      'entity_type' => 'election_candidate',
      'field_name' => 'field_irc_handle',
      'label' => 'IRC handle',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'text_textfield',
        'weight' => '10',
      ),
    ),
  );

  // Exported field: 'election_candidate-candidate-field_meet_the_candidate_session'.
  $fields['election_candidate-candidate-field_meet_the_candidate_session'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '2',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_meet_the_candidate_session',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'wednesday' => 'I am available to participate in a Meet the Candidates conference call on <strong>Wednesday, Sept. 11th 2013 at 01:00 UTC</strong><br/>5 PM PST Tuesday Sept. 10th in the US and Canada<br/>8 PM EST Tuesday Sept. 10th in the US and Canada <br/>11 PM Tuesday Sept. 10th in Sao Paulo Brasil <br/>1 AM Wednesday Sept. 11th in London <br/>9 AM Wednesday Sept. 11th in Beijing <br/>12 noon Wednesday Sept. 11th in Sydney Australia.',
          'thursday' => 'I am available to participate in a Meet the Candidates conference call on <strong>Thursday, September 12 2013 at 17:00 UTC</strong><br/>9 AM PST Thursday, September 12 in the US and Canada<br/>12 noon EST Thursday, September 12 in the US and Canada<br/>3 PM Thursday, September 12 in Sao Paulo Brasil<br/>5 PM Thursday, September 12 in London<br/>1 AM Friday, September 13 in Beijing<br/>4 AM Friday, September 13 in Sydney Australia.',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'candidate',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '11',
        ),
        'details' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '13',
        ),
        'full' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '11',
        ),
      ),
      'entity_type' => 'election_candidate',
      'field_name' => 'field_meet_the_candidate_session',
      'label' => 'Meet the Candidate Sessions',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '11',
      ),
    ),
  );

  // Exported field: 'election_candidate-candidate-field_motivation'.
  $fields['election_candidate-candidate-field_motivation'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_motivation',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'allowed_values' => '',
        'allowed_values_php' => '',
        'max_length' => '',
        'text_processing' => '1',
      ),
      'translatable' => '0',
      'type' => 'text_long',
      'type_name' => 'nomination',
    ),
    'field_instance' => array(
      'bundle' => 'candidate',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Why are you applying for a board position? What initiatives do you hope to help drive, or what perspectives are you going to try and represent?',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '9',
        ),
        'details' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '8',
        ),
        'full' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '5',
        ),
      ),
      'entity_type' => 'election_candidate',
      'field_name' => 'field_motivation',
      'label' => 'Motivation',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '4',
        ),
        'type' => 'text_textarea',
        'weight' => '6',
      ),
    ),
  );

  // Exported field: 'election_candidate-candidate-field_travel'.
  $fields['election_candidate-candidate-field_travel'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_travel',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          0 => 'I am unable to travel to four in-person board meetings per year, regardless of financial sponsorship',
          1 => 'I am able to travel to four in-person board meetings per year (either self-funded, or with financial sponsorship)',
        ),
        'allowed_values_function' => '',
        'allowed_values_php' => '',
        'max' => '',
        'min' => '',
        'prefix' => '',
        'suffix' => '',
      ),
      'translatable' => '0',
      'type' => 'list_boolean',
      'type_name' => 'nomination',
    ),
    'field_instance' => array(
      'bundle' => 'candidate',
      'default_value' => array(
        0 => array(
          'value' => 0,
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '15',
        ),
        'details' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '14',
        ),
        'full' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '15',
        ),
      ),
      'entity_type' => 'election_candidate',
      'field_name' => 'field_travel',
      'label' => 'Travel',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 0,
        ),
        'type' => 'options_onoff',
        'weight' => '8',
      ),
    ),
  );

  // Exported field: 'election_candidate-candidate-field_twitter'.
  $fields['election_candidate-candidate-field_twitter'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_twitter',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'candidate',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Enter your twitter handle, in the form @username',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '6',
        ),
        'details' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '4',
        ),
        'full' => array(
          'label' => 'above',
          'module' => 'twitterfield',
          'settings' => array(),
          'type' => 'twitterfield_twitter_link',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'election_candidate',
      'field_name' => 'field_twitter',
      'label' => 'Twitter handle',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'twitterfield',
        'settings' => array(
          'allowed_types' => array(
            'hashtag' => 0,
            'list' => 0,
            'search' => 0,
            'username' => 'username',
          ),
          'size' => '60',
        ),
        'type' => 'twitterfield',
        'weight' => '9',
      ),
    ),
  );

  // Exported field: 'reply-candidate_comments-field_comment'.
  $fields['reply-candidate_comments-field_comment'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_comment',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'candidate_comments',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'reply',
      'field_name' => 'field_comment',
      'label' => 'Comment',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '5',
        ),
        'type' => 'text_textarea',
        'weight' => '1',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('About Me');
  t('Comment');
  t('Comments');
  t('Enter your twitter handle, in the form @username');
  t('Experience');
  t('IRC handle');
  t('Meet the Candidate Sessions');
  t('Motivation');
  t('Picture');
  t('Tell us about yourself! Your background, how you got into Drupal, etc. ');
  t('Travel');
  t('Twitter handle');
  t('What Drupal community contributions have you take part in (code, camps, etc.)? Do you have experience in financial oversight, developing business strategies, or organization governance? ');
  t('Why are you applying for a board position? What initiatives do you hope to help drive, or what perspectives are you going to try and represent?');

  return $fields;
}
