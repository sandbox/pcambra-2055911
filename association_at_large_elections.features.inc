<?php
/**
 * @file
 * association_at_large_elections.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function association_at_large_elections_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function association_at_large_elections_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_default_reply_bundle().
 */
function association_at_large_elections_default_reply_bundle() {
  $items = array();
  $items['candidate_comments'] = entity_import('reply_bundle', '{
    "bundle" : "candidate_comments",
    "name" : "Comment to candidate",
    "access" : "2",
    "display" : "2",
    "description" : "",
    "form" : "1",
    "allow_reply" : "1",
    "locked" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}
