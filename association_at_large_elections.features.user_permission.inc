<?php
/**
 * @file
 * association_at_large_elections.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function association_at_large_elections_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer candidate types.
  $permissions['administer candidate types'] = array(
    'name' => 'administer candidate types',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election_candidate',
  );

  // Exported permission: administer elections.
  $permissions['administer elections'] = array(
    'name' => 'administer elections',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election',
  );

  // Exported permission: administer replies.
  $permissions['administer replies'] = array(
    'name' => 'administer replies',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'reply',
  );

  // Exported permission: administer reply bundles.
  $permissions['administer reply bundles'] = array(
    'name' => 'administer reply bundles',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'reply',
  );

  // Exported permission: bypass running election lock.
  $permissions['bypass running election lock'] = array(
    'name' => 'bypass running election lock',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election',
  );

  // Exported permission: create elections.
  $permissions['create elections'] = array(
    'name' => 'create elections',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election',
  );

  // Exported permission: delete any election.
  $permissions['delete any election'] = array(
    'name' => 'delete any election',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election',
  );

  // Exported permission: delete candidate_comments reply.
  $permissions['delete candidate_comments reply'] = array(
    'name' => 'delete candidate_comments reply',
    'roles' => array(),
    'module' => 'reply',
  );

  // Exported permission: delete own candidate_comments reply.
  $permissions['delete own candidate_comments reply'] = array(
    'name' => 'delete own candidate_comments reply',
    'roles' => array(),
    'module' => 'reply',
  );

  // Exported permission: delete own elections.
  $permissions['delete own elections'] = array(
    'name' => 'delete own elections',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election',
  );

  // Exported permission: edit any election.
  $permissions['edit any election'] = array(
    'name' => 'edit any election',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election',
  );

  // Exported permission: edit candidate_comments reply.
  $permissions['edit candidate_comments reply'] = array(
    'name' => 'edit candidate_comments reply',
    'roles' => array(),
    'module' => 'reply',
  );

  // Exported permission: edit own candidate_comments reply.
  $permissions['edit own candidate_comments reply'] = array(
    'name' => 'edit own candidate_comments reply',
    'roles' => array(),
    'module' => 'reply',
  );

  // Exported permission: edit own elections.
  $permissions['edit own elections'] = array(
    'name' => 'edit own elections',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election',
  );

  // Exported permission: edit own nominations.
  $permissions['edit own nominations'] = array(
    'name' => 'edit own nominations',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'election_candidate',
  );

  // Exported permission: export any election results.
  $permissions['export any election results'] = array(
    'name' => 'export any election results',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election_export',
  );

  // Exported permission: export own election results.
  $permissions['export own election results'] = array(
    'name' => 'export own election results',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election_export',
  );

  // Exported permission: post candidate_comments reply.
  $permissions['post candidate_comments reply'] = array(
    'name' => 'post candidate_comments reply',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'reply',
  );

  // Exported permission: submit nominations.
  $permissions['submit nominations'] = array(
    'name' => 'submit nominations',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'election_candidate',
  );

  // Exported permission: undo own vote.
  $permissions['undo own vote'] = array(
    'name' => 'undo own vote',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'election_vote',
  );

  // Exported permission: view any election results.
  $permissions['view any election results'] = array(
    'name' => 'view any election results',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'election_results',
  );

  // Exported permission: view candidate_comments reply.
  $permissions['view candidate_comments reply'] = array(
    'name' => 'view candidate_comments reply',
    'roles' => array(
      0 => 'anonymous user',
      1 => 'authenticated user',
    ),
    'module' => 'reply',
  );

  // Exported permission: view election statistics.
  $permissions['view election statistics'] = array(
    'name' => 'view election statistics',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'election_statistics',
  );

  // Exported permission: view own election results.
  $permissions['view own election results'] = array(
    'name' => 'view own election results',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'election_results',
  );

  // Exported permission: view published elections.
  $permissions['view published elections'] = array(
    'name' => 'view published elections',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'election',
  );

  // Exported permission: vote in elections.
  $permissions['vote in elections'] = array(
    'name' => 'vote in elections',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'election_vote',
  );

  return $permissions;
}
