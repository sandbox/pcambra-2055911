<?php

/**
 * Implements hook_schema().
 */
function association_at_large_elections_schema() {
  $schema = array();

  $schema['association_at_large_elections_electorate'] = array(
    'description' => 'Electorate eligible for voting in the at large elections',
    'fields' => array(
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary Key: Unique user ID.',
        'default' => 0,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 60,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Unique user name.',
      ),
    ),
    'primary key' => array('uid'),
  );

  return $schema;
}

/**
 * Enable dependent modules.
 */
function association_at_large_elections_update_7001(&$sandbox) {
  $modules = array('election', 'election_candidate', 'election_condition',
    'election_export', 'election_post', 'election_results', 'election_statistics',
    'election_stv', 'election_vote', 'reply', 'strongarm', 'twitterfield',
    'views_bulk_operations');
  module_enable($modules);
}

/**
 * Place the nomination block in the region.
 */
function association_at_large_elections_update_7002(&$sandbox) {
  $values = array(
    array(
      'module' => 'association_at_large_elections',
      'delta' => 'ado_election_nomination',
      'theme' => 'bluecheese',
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_second',
      'pages' => 'election-post*',
      'cache' => -1,
    ),
  );
  $query = db_insert('block')->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'cache'));
  foreach ($values as $record) {
    $query->values($record);
  }
  $query->execute();
}

/**
 * Make sure that the electorate table is there.
 */
function association_at_large_elections_update_7003(&$sandbox) {
  if (db_table_exists('association_at_large_elections_electorate') == FALSE) {
    drupal_install_schema('association_at_large_elections');
  }
}

/**
 * Hide nomination block for anonymous.
 */
function association_at_large_elections_update_7004(&$sandbox) {
  $values = array(
    array(
      'module' => 'association_at_large_elections',
      'delta' => 'ado_election_nomination',
      'rid' => DRUPAL_AUTHENTICATED_RID,
    ),
  );
  $query = db_insert('block_role')->fields(array('module', 'delta', 'rid'));
  foreach ($values as $record) {
    $query->values($record);
  }
  $query->execute();
}
