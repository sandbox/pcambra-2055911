<?php
/**
 * @file
 * association_at_large_elections.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function association_at_large_elections_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_election_candidate__candidate';
  $strongarm->value = array(
    'view_modes' => array(
      'details' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => TRUE,
      ),
      'teaser' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'post' => array(
          'weight' => '0',
        ),
        'first_name' => array(
          'weight' => '1',
        ),
        'last_name' => array(
          'weight' => '2',
        ),
        'mail' => array(
          'weight' => '3',
        ),
      ),
      'display' => array(
        'election' => array(
          'default' => array(
            'weight' => '14',
            'visible' => FALSE,
          ),
          'details' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '14',
            'visible' => FALSE,
          ),
        ),
        'post' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'details' => array(
            'weight' => '0',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '8',
            'visible' => FALSE,
          ),
        ),
        'first_name' => array(
          'default' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
          'details' => array(
            'weight' => '1',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '9',
            'visible' => FALSE,
          ),
        ),
        'last_name' => array(
          'default' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'details' => array(
            'weight' => '2',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
        ),
        'mail' => array(
          'default' => array(
            'weight' => '13',
            'visible' => FALSE,
          ),
          'details' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '13',
            'visible' => FALSE,
          ),
        ),
        'username' => array(
          'default' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'details' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
          'full' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
        'status' => array(
          'default' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
          'details' => array(
            'weight' => '11',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_election_candidate__candidate'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_reply__candidate_comments';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_reply__candidate_comments'] = $strongarm;

  return $export;
}
